import { CursosService } from './../cursos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cursos-detalhe',
  templateUrl: './cursos-detalhe.component.html',
  styleUrls: ['./cursos-detalhe.component.css']
})
export class CursosDetalheComponent implements OnInit {


  cursos:string[];

  constructor(private cursosService: CursosService) {
    // for(let i=0; i<this.cursos.length; i++){
    //   let curso = this.cursos[i];
    // }

    // var servico = new CursosService();
    
    this.cursos = this.cursosService.getCursos();

   }

  ngOnInit(): void {
  }

}
