import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  url: string = 'http://loiane.com';
  cursoAngular: boolean = true;
  urlImagem: string[] = [
    'https://image.freepik.com/vetores-gratis/pessoa-com-ponteiro-mostrando-graficos-para-idosos_157667-28.jpg',
    'https://image.freepik.com/fotos-gratis/nota-de-cem-dolares-australiano_38165-98.jpg',
  ]

  getValor(){
    return 1;
  }

  getCurtirCurso(){
    return true;
  }


  constructor() {
    for (let i = 0; i < this.urlImagem.length; i++) {
      const imagem = this.urlImagem[i];
      
    }
   }



  ngOnInit(): void {
  }

}
